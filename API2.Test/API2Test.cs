using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;
using Newtonsoft.Json;
using soft.API2;

namespace soft.API1.Test
{
    public class TaxaJurosTest
    {
        private readonly HttpClient _client;

        public TaxaJurosTest()
        {
            // Cria um mini web server como wrapper da API
            var server = new TestServer(new WebHostBuilder()
                .UseEnvironment("Development")
                .UseStartup<Startup>());

            _client = server.CreateClient();
        }

        [Theory]
        [InlineData("GET")]
        public async Task GetCalculoJuros(string method)
        {
            var request = new HttpRequestMessage(new HttpMethod(method), "/calculaJuros?valorInicial=100&meses=5");

            var response = await _client.SendAsync(request);

            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync();
            var valorFinal = JsonConvert.DeserializeObject<double>(stringResponse);
            Assert.True(valorFinal.Equals(105.10));
        }

        [Theory]
        [InlineData("GET")]
        public async Task GetTheCode(string method)
        {
            var request = new HttpRequestMessage(new HttpMethod(method), "/showmethecode/");

            var response = await _client.SendAsync(request);

            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync();
            Assert.True(stringResponse == "https://github.com/edrago/soft-dotnetcore");
        }
    }
}
