﻿﻿﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace soft.API2.Controllers
{ 
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class CalculaJurosController : ControllerBase
    {
        private readonly ILogger<CalculaJurosController> _logger;

        public CalculaJurosController(ILogger<CalculaJurosController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Consulta a taxa de juros da API 1 e retorna valor baseado no valor inicial e meses informados.
        /// </summary>
        [HttpGet]
        public async Task<decimal> Get([FromQuery(Name = "valorinicial")] double valorInicial, [FromQuery(Name = "meses")] int meses)
        {
            double taxaJuros;
            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual;
            handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
            {
                return true;
            };
            var httpClient = new HttpClient(handler);

            try
            {
                var response = await httpClient.GetAsync("https://api1-softplan.azurewebsites.net/TaxaJuros");

                if (response.StatusCode != HttpStatusCode.OK) {
                    throw new WebException("Houve um problema na consulta de juros");
                }

                var retorno = await response.Content.ReadAsStringAsync();

                if (retorno.Length == 0 || !double.TryParse(retorno, out taxaJuros)) {
                    throw new WebException("Houve um problema na consulta de juros");
                }

                var valorFinal = valorInicial * Math.Pow(1 + 0.01, meses);
                return decimal.Parse(valorFinal.ToString("#.00"), System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (System.Exception)
            {
                throw new WebException("Houve um problema na consulta de juros");
            }
        }
    }
}
