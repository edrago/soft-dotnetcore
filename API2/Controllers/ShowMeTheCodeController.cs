using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace soft.API2.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class ShowMeTheCodeController : ControllerBase
    {
        private readonly ILogger<ShowMeTheCodeController> _logger;

        public ShowMeTheCodeController(ILogger<ShowMeTheCodeController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Retorna URL com endereço do repo com o sourcecode.
        /// </summary>
        [HttpGet]
        public string Get()
        {
            return "https://github.com/edrago/soft-dotnetcore";
        }
    }
}
