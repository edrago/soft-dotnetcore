﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace soft.API1.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class TaxaJurosController : ControllerBase
    {
        private readonly ILogger<TaxaJurosController> _logger;

        public TaxaJurosController(ILogger<TaxaJurosController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Retorna a taxa de juros
        /// </summary>
        [HttpGet]
        public double Get()
        {
            return 0.01;
        }
    }
}
